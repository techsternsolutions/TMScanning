﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TMScanning.Infrastructure
{
    public class FileUploadResult
    {
        public string LocalFilePath { get; set; }
        public string FileName { get; set; }
        public long FileLength { get; set; }
        public int SchoolID { get; set; }
        public string Message { get; set; }
    }
}
